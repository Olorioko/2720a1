#ifndef __SQUARE_H
#define __SQUARE_H


#include "Simulator.h"
#include "Drawable.h"
#include "Moveable.h"
#include "SimulatorMine.h"
#include <memory>
#include <allegro5\allegro_primitives.h>



struct Point {
	double x;
	double y;
Point(double a=0.0, double b=0.0): x(a), y(b) {};
	Point operator + (Vector v) {
		return Point(x+v.x, y+v.y);
	}
};


//Purpose: Create a circle object
class Circle: public Drawable, public Moveable{
private:
	Point origin; // the origin of the circle
	Vector crtSpeed; // speed in pixels per sec
	int width, height, radius; // of the window

public:
//Constructor
//Input: Display , frame persecond, circle size
//return: void
Circle(const Display & d, int fps, int cr) : 
		Simulator(d, fps),
		origin(400, 300),
		crtSpeed(300, 600), 
	{ 
		width = o.getW(); height = d.getH(); 
	}

	//Change the position of each instance of circle
	//Input: Dimension to move by (dt)
	//return: void
	void deltaMove(double dt) {
		Point newOrigin = origin + crtSpeed*dt;
		if (newOrigin.x < 0 || newOrigin.x > width-squareSize) {
			crtSpeed = crtSpeed.reflectOverYAxis();
			if (newOrigin.x < 0) {
				newOrigin.x = -newOrigin.x;  // ugly reflection
			} else {
				newOrigin.x = 2*width -2*squareSize - newOrigin.x; // also ugly
			}			
		}

		if (newOrigin.y < 0 || newOrigin.y > height-squareSize) {
			crtSpeed = crtSpeed.reflectOverXAxis();
			if (newOrigin.y < 0) {
				newOrigin.y = -newOrigin.y;
			}
			else {
				newOrigin.y = 2*height -2*squareSize - newOrigin.y;
			}
		}
		origin = newOrigin;
	}
	//Draw the circle shappe
	//Input: none
	//return: void
	void draw() {
		al_draw_cicle (100, 100, 5, )
		
	}

};

#endif