#ifndef __SIMULATORMINE_H
#define __SIMULATORMINE_H

#include <memory>
#include <list>
#include "Moveable.h"
#include "Drawable.h"
#include "Simulator.h"
#include <allegro5/allegro.h>
#include <vector>


using namespace std;


class SimulatorMine : public Simulator{
private:
	list <shared_ptr<Moveable>> move;
	list <shared_ptr<Drawable>> draw;

public:
	//Constructor
	SimulatorMine(const Display & d, int fps) : Simulator(d, fps) {}
	//Destructor
	~SimulatorMine(){};

	//Adding objects to the shared ptr list 
	//Input: type double i
	//return: void
	void updateModel(double dt){
		for (list <shared_ptr<Moveable>>::iterator it = move.begin(); it != move.end(); it++){
			(*it)->deltaMove(dt);
		}
	}


	//Iterating though the draw objects in the list 
	//Input: void
	//return: void
 	void drawModel(){
		al_clear_to_color(al_map_rgb(0, 0, 0));
		for (list <shared_ptr<Drawable>>::iterator it = draw.begin(); it != draw.end(); it++){
			(*it)->draw();
			
		}
		al_flip_display();
	}
	 
	//Adding objects to the shared ptr list 
	//Input: shared ptr of type moveable
	//return: void
	 void addMoveable (shared_ptr<Moveable>moveptr){
		move.push_back(moveptr);
	 }

	 //Adding draw objects to the shared ptr list 
	 //Input: shared ptr of type Drawable
	 //return: void
	 void addDrawable(shared_ptr<Drawable>drawptr){
	 	draw.push_back(drawptr);
	 }
};



#endif