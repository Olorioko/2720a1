#ifndef __CIRCLE_H
#define __CIRCLE_H
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include "Simulator.h"
#include "Drawable.h"
#include "Moveable.h"
#include "SimulatorMine.h"
#include <memory>
#include "Square.h"



using namespace std;

//Purpose: Create a circle object
class Circle: public Drawable, public Moveable{
private:
	Point origin; // the origin of the circle
	Vector crtSpeed; // speed in pixels per sec
	int width, height, squareSize; // of the window

public:
//Constructor
//Input: Display , frame persecond, circle radius
//return: void
Circle(const Display & d, int fps, int sq) : 
	
		origin(0, 500),
		crtSpeed(300, 300), 
		squareSize (sq)
	{ 
		width = d.getW(); 
		height = d.getH(); 
	}

	//Change the position of each instance of circle
	//Input: Dimension to move by (dt)
	//return: void
	virtual void deltaMove(double dt) 
	{
		Point newOrigin = origin + crtSpeed*dt;

		if (newOrigin.x > 800 + squareSize)
		{newOrigin.x = newOrigin.x - 800 - squareSize; }


			if(newOrigin.x > 800 + squareSize)
				{newOrigin.x=newOrigin.x -800 - squareSize;}
		origin = newOrigin;
	}

	//Draw the circle shappe
	//Input: none
	//return: void
	void draw() {
		al_draw_circle(100, 100, 5, al_map_rgb(250,200,200),2 );
			/*al_draw_circle(static_cast<int>(origin.x),
											static_cast<int>(origin.y),
											static_cast<int>(origin.x)+squareSize,
											static_cast<int>(origin.y),
											al_map_rgb(200, 200, 0),
											2);*/
	}

};

#endif