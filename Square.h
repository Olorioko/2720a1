//Arthor: Awoderu Omotoyosi
//Course: CPSC 2720

#ifndef __SQUARE_H
#define __SQUARE_H
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include "Simulator.h"
#include "Drawable.h"
#include "Moveable.h"
#include "SimulatorMine.h"
#include <memory>

using namespace std;


struct Vector {
	double x;
	double y;
	Vector (double a=0.0, double b=0.0) : x(a), y(b) {};

	Vector operator * (double scalar) {
		return Vector(x*scalar, y*scalar);
	}

	Vector reflectOverXAxis() {
		return Vector(x, -y);
	}

	Vector reflectOverYAxis() {
		return Vector(-x, y);
	}
};


struct Point {
	double x;
	double y;
Point(double a=0.0, double b=0.0): x(a), y(b) {};
	Point operator + (Vector v) {
		return Point(x+v.x, y+v.y);
	}
};


//Purpose: To create a square object 
class Square: public Drawable, public Moveable{
private:
	Point origin; // the origin of the square
	Vector crtSpeed; // speed in pixels per sec
	int width, height; // of the window
	int squareSize; // of the drawn block in pixels


public:
	//Constructor
	//Input: Display , frame persecond, square size
	//return: void
	Square(const Display & d, int fps, int sq):
		origin(0, 500), 
		crtSpeed(400, 0), 
		squareSize(sq)
	{ 
		width = d.getW(); 
		height = d.getH(); 
	}

	//Change the position of each instance of square
	//Input: change the coordinate of square (dt)
	//return: void
	virtual void deltaMove(double dt) 
	{
		Point newOrigin = origin + crtSpeed*dt;


		if (newOrigin.x > 800 + squareSize)
		{newOrigin.x = newOrigin.x - 800 - squareSize; }


			if(newOrigin.x > 800 + squareSize)
				{newOrigin.x=newOrigin.x -800 - squareSize;}
		origin = newOrigin;
	}

	//Draw the square shape
	//Input: none
	//return: void
	virtual void draw() 
	{
		al_draw_rectangle(static_cast<int>(origin.x),
											static_cast<int>(origin.y),
											static_cast<int>(origin.x)+squareSize,
											static_cast<int>(origin.y)+squareSize,
											al_map_rgb(200, 200, 0),
											2);
		
		
	}

};

#endif