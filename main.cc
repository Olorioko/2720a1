#include <iostream>
#include "Display.h"
#include "SimulatorMine.h"
#include "Moveable.h"
#include "Drawable.h"
#include "Square.h"
#include "Ground.h"
#include "Circle.h"
#include <memory> 
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
using namespace std;
 
int main(void) {
	Display disp;  // create a 800x600 window
	SimulatorMine simul(disp, 120);
	//shared_ptr<Ground> grdptr;
	shared_ptr <Square> temp = make_shared<Square> (disp, 10,10);
	shared_ptr<Moveable> Movetemp =  temp;  
	shared_ptr<Drawable> Drawtemp =  temp;
	simul.addMoveable(Movetemp);
	simul.addDrawable(Drawtemp); 
	SimulatorMine simul1(disp, 120);
	shared_ptr <Circle> temp1 = make_shared<Circle> (disp, 10,10);
	shared_ptr<Moveable> Movetemp1 =  temp1;  
	shared_ptr<Drawable> Drawtemp1 =  temp1;
	simul.addMoveable(Movetemp1);
	simul.addDrawable(Drawtemp1); 

	
	//Ground object
	shared_ptr<Ground> temp2 = make_shared<Ground> (disp,120,10);
	shared_ptr<Drawable> Drawtemp2 =  temp2;
	simul.addDrawable(Drawtemp2); 

	//shared_ptr<Drawable>temp2 (new Ground());
	simul.run();      
	
}             
