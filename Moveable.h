
#ifndef __MOVEABLE_H
#define __MOVEABLE_H

class Moveable {
public:
	virtual ~Moveable(){}
	virtual void deltaMove(double) = 0;
};
#endif 