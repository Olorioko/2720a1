#ifndef __DRAWABLE_H
#define __DRAWABLE_H

class Drawable {
public:
	virtual ~Drawable() {}
	virtual void draw() = 0 ;
};
#endif
