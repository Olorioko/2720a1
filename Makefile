CFLAGS = -std=c++11 -I /home/lib2720/allegro5/include/
LIBDIR = /home/lib2720/allegro5/lib/
LNFLAGS = -lallegro -lallegro_primitives
OBJECTS = Display.o main.o Simulator.o
all: clean-all show run

show: $(OBJECTS)
	g++11 -L $(LIBDIR) -o $@ $^ $(LNFLAGS) 

%.o : %.cc
	g++11 $(CFLAGS) -c $^

clean:
	rm -f *.o *~ *% .#*
clean-all:
	rm -f show *.o *~ *% *# .#*

run:
	show